#include "MyTest.h"

#include "HggExpPdf.h"
#include "RooCBShape.h"

int main() {

  MyTest my_test;

  my_test.print();

  auto mypdf = new HggExpPdf();
  auto pdf = new RooCBShape();

  return 0;
}
