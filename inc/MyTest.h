#include "TObject.h"

class MyTest : public TObject {

public:
  void print();

private:
  ClassDef(MyTest, 2); // The class title
};
