

#ifndef HGGEXPPDF_H
#define HGGEXPPDF_H

#include "RooAbsCategory.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooCategoryProxy.h"
#include "RooRealProxy.h"

class HggExpPdf : public RooAbsPdf {
public:
  HggExpPdf(){};

  HggExpPdf(const char *name, const char *title, RooAbsReal &_m,
            RooAbsReal &_a);

  HggExpPdf(const HggExpPdf &other, const char *name = 0);

  virtual TObject *clone(const char *newname) const {
    return new HggExpPdf(*this, newname);
  }
  
  inline virtual ~HggExpPdf() {}

protected:
  RooRealProxy m;
  RooRealProxy a;

  Double_t evaluate() const;

private:
  ClassDef(HggExpPdf, 2);
};

#endif
