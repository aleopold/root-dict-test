#include "HggExpPdf.h"

#include "Riostream.h"
#include "RooAbsCategory.h"
#include "RooAbsReal.h"
#include "TMath.h"
#include <math.h>

ClassImp(HggExpPdf);

HggExpPdf::HggExpPdf(const char *name, const char *title, RooAbsReal &_m,
                     RooAbsReal &_a)
    : RooAbsPdf(name, title), m("m", "m", this, _m), a("a", "a", this, _a) {}

HggExpPdf::HggExpPdf(const HggExpPdf &other, const char *name)
    : RooAbsPdf(other, name), m("m", this, other.m), a("a", this, other.a) {}

Double_t HggExpPdf::evaluate() const {
  double x = m / 13000.;
  return exp(a * x);
}
