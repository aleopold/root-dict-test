export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

lsetup "cmake 3.14.0"
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
lsetup "python 2.7.4-x86_64-slc6-gcc48"

export ROOTFOLDERPATH=`pwd`
export ROOTDATAPATH="${ROOTFOLDERPATH}/data"
export ROOTCFGPATH="${ROOTFOLDERPATH}/cfg"

