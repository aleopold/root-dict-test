ROOTDIR=`pwd`
CONFIGS="release"
for CONFIG in $CONFIGS; do
  BUILDFOLDER=$ROOTDIR/build/

  rm -rf $BUILDFOLDER
  mkdir -p $BUILDFOLDER

  cd $BUILDFOLDER

  cmake  $ROOTDIR || return 1

  make

  if [ $? -eq 0 ]
  then
    ctest
  fi

  cd $ROOTDIR
done

cd $ROOTDIR

