cmake_minimum_required (VERSION 3.10)

project (Project)

##### You need to tell CMake where to find the ROOT installation.
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS} -lTreePlayer -O3 -std=c++17")

set(CMAKE_CXX_STANDARD 17)

message( ${CMAKE_CXX_FLAGS} )

# find_package(ROOT REQUIRED COMPONENTS RIO Hist Tree RooFitCore RooFit RooStats MathCore Core)
find_package(ROOT 6.18 CONFIG REQUIRED COMPONENTS RIO Hist Tree Core MathCore RooFit RooFitCore RooStats)
#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIR})

#Bring the headers into the project
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)

########## VERSION 1 ##########
# ROOT_GENERATE_DICTIONARY(RootDictionary
#    MyTest.h
#    LINKDEF LinkDef.h)
# add_library(RootDictLib SHARED src/MyTest.cxx RootDictionary.cxx)
# target_link_libraries(RootDictLib ${ROOT_LIBRARIES})
###############################
########## VERSION 2 ##########
ROOT_GENERATE_DICTIONARY(RootDictionary
   MyTest.h
   HggExpPdf.h
   LINKDEF LinkDef.h)
add_library(RootDictLib SHARED src/MyTest.cxx src/HggExpPdf.cxx RootDictionary.cxx)
target_link_libraries(RootDictLib ${ROOT_LIBRARIES})
###############################



#To include a directory, that contains a CMakeLists file do:
add_subdirectory(src)
add_subdirectory(exe)

### testing
enable_testing()
add_subdirectory(test)
